package tdd.training.mra;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import tdd.training.mra.MarsRoverStatus.Directions;

public class MarsRover {

	private int planetX;
	private int planetY;
	private List<String> planetObstacles;
	private MarsRoverStatus roverStatus;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.roverStatus = new MarsRoverStatus(0, 0, MarsRoverStatus.Directions.N);
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return this.planetObstacles.contains("(" + x + "," + y + ")");
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {

		if (commandString.isEmpty())
			return "(0,0,N)";
		else {
			
			Set<String> encounteredObstacles = new LinkedHashSet<String>();
			MarsRoverStatus previousRoverStatus;
			Directions[] directionList = Directions.values();
			
			String[] commands = commandString.split("");
			for (String command : commands) {
				previousRoverStatus = new MarsRoverStatus(this.roverStatus.getXPos(), this.roverStatus.getYPos(), this.roverStatus.getDirection());
				if (command.equals("r")) {
					// directionList = [N,E,S,W] => directionList[3] == W, then directionList[(3+1) % 4] == N
					this.roverStatus.setDirection(directionList[(this.roverStatus.getDirection().ordinal() + 1) % 4]);
					}
				else if (command.equals("l")) {
					// directionList = [N,E,S,W] => directionList[0] == N, then directionList[(0+3) % 4] == W
					this.roverStatus.setDirection(directionList[(this.roverStatus.getDirection().ordinal() + 3) % 4]);
			}
				else if (command.equals("f")) {
					switch (this.roverStatus.getDirection().toString()) {
					case "N":
						this.increaseYPosition();
						break;
					case "E":
						this.increaseXPosition();
						break;
					case "S":
						this.decreaseYPosition();
						break;
					case "W":
						this.decreaseXPosition();
						break;
					}
				} else if (command.equals("b")) {
					switch (this.roverStatus.getDirection().toString()) {
					case "N":
						this.decreaseYPosition();
						break;
					case "E":
						this.decreaseXPosition();
						break;
					case "S":
						this.increaseYPosition();
						break;
					case "W":
						this.increaseXPosition();
						break;
					}
				}
				
				//può capitare solo dopo "f" o "b"
				if(this.planetContainsObstacleAt(this.roverStatus.getXPos(), this.roverStatus.getYPos()))
				{
						encounteredObstacles.add(this.roverStatus.getCoordinates());
						this.roverStatus.setXPos(previousRoverStatus.getXPos());
						this.roverStatus.setYPos(previousRoverStatus.getYPos());
				}
			}
			// eliminating ',' between obstacles caused by LinkedHashSet.toString()
			String resultStatus = this.getStatus().concat(encounteredObstacles.toString()).replaceAll("\\)\\,", ")");
			// eliminating '[', ']' and ' ' caused by LinkedHashSet.toString()
			return resultStatus.replaceAll("\\[|\\]|\\s+", "");
		}
	}
	
	/**
	 * It updates the status of the rover.
	 * 
	 * @param newRoverStatus A string that can contain the new status of the rover
	 * 
	 */
	public void setStatus(String newStatus) {
		//newStatus == "(5,6,N)"
		int x = Integer.parseInt(""+newStatus.charAt(1));
		int y = Integer.parseInt(""+newStatus.charAt(3));
		
		this.roverStatus.setStatus(x, y, Directions.valueOf(""+newStatus.charAt(5)));
	}

	/**
	 * It returns the actual status of the rover.
	 * 
	 * @return the actual status of the rover
	 */
	public String getStatus() {
		return this.roverStatus.toString();
	}
	
	public String getDirection() {
		return this.roverStatus.getDirection().toString();
	}


	public void increaseXPosition() {
		int xPosition = this.roverStatus.getXPos();
		//increasing and wrapping of x mars rover coordinate
		xPosition = (xPosition + 1)%this.planetX;
		this.roverStatus.setXPos(xPosition);
	}

	public void decreaseXPosition() {
		int xPosition = this.roverStatus.getXPos();
		//decreasing and wrapping of x mars rover coordinate
		//wrapping => 0 x_position == this.planetY position => 0 x_position - 1 step == this.planetX - 1 => (this.planetX-1) == -1 step
		xPosition = (xPosition + (this.planetX-1))%this.planetX;
		this.roverStatus.setXPos(xPosition);;
	}
	
	public void increaseYPosition() {
		int yPosition = this.roverStatus.getYPos();
		//increasing and wrapping of y mars rover coordinate
		yPosition = (yPosition + 1)%this.planetY;
		this.roverStatus.setYPos(yPosition);
	}

	public void decreaseYPosition() {
		int yPosition = this.roverStatus.getYPos();
		//increasing and wrapping of y mars rover coordinate
		//wrapping => 0 y_position == this.planetY position => 0 y_position - 1 step == this.planetY - 1 => (this.planetY-1) == -1 step
		yPosition = (yPosition + (this.planetY-1))%this.planetY; 
		this.roverStatus.setYPos(yPosition);
	}


}
