package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MarsRoverTest {

	private List<String> planetObstacles;
	private MarsRover rover;
	private String command, roverStatus;

	@Before
	public void setUp() throws MarsRoverException {
		planetObstacles = new ArrayList<String>();
		rover = new MarsRover(10, 10, planetObstacles);
		//Initial landing position
		this.rover.setStatus("(0,0,N)");
	}

	// Adding tests
	@Test
	public void testGetDirection() throws MarsRoverException {
		rover.setStatus("(5,8,E)");
		// testing MarsRover.getDirection()
		assertEquals("Failure of getDirection() method.", "E", this.rover.getDirection());
	}

	@Test
	public void testGetStatus() throws MarsRoverException {
		//Initial landing position
		
		// testing MarsRover.getStatus()
		assertEquals("Failure of getStatus() method.", "(0,0,N)", this.rover.getStatus());
	}

	@Test
	public void testIncreaseXPosition() throws MarsRoverException {
		//Initial landing position

		// testing MarsRover.increaseXPosition()
		rover.increaseXPosition();
		assertEquals("Failure of increaseXPosition() method.", "(1,0,N)", this.rover.getStatus());
		// testing wrapping of MarsRover.increaseXPosition()
		rover.setStatus("(9,0,N)");
		rover.increaseXPosition();
		assertEquals("Failure of increaseXPosition() method.", "(0,0,N)", this.rover.getStatus());
	}

	@Test
	public void testDecreaseXPosition() throws MarsRoverException {
		rover.setStatus("(1,0,N)");
		// testing MarsRover.decreaseXPosition()
		rover.decreaseXPosition();
		assertEquals("Failure of decreaseXPosition() method.", "(0,0,N)", this.rover.getStatus());
		// testing wrapping of MarsRover.decreaseXPosition()
		rover.decreaseXPosition();
		assertEquals("Failure of decreaseXPosition() method.", "(9,0,N)", this.rover.getStatus());
	}

	@Test
	public void testIncreaseYPosition() throws MarsRoverException {
		//Initial landing position

		// testing MarsRover.increaseYPosition()
		rover.increaseYPosition();
		assertEquals("Failure of increaseYPosition() method.", "(0,1,N)", this.rover.getStatus());
		// testing wrapping of MarsRover.increaseYPosition()
		rover.setStatus("(0,9,N)");
		rover.increaseYPosition();
		assertEquals("Failure of increaseYPosition() method.", "(0,0,N)", this.rover.getStatus());
	}

	@Test
	public void testDecreaseYPosition() throws MarsRoverException {
		rover.setStatus("(0,1,N)");
		// testing MarsRover.decreaseYPosition()
		rover.decreaseYPosition();
		assertEquals("Failure of decreaseYPosition() method.", "(0,0,N)", this.rover.getStatus());
		// testing wrapping of MarsRover.decreaseYPosition()
		rover.setStatus("(0,0,N)");
		rover.decreaseYPosition();
		assertEquals("Failure of increaseYPosition() method.", "(0,9,N)", this.rover.getStatus());
	}

	// User Story Tests

	@Test
	public void testPlanetContainsObstacleAt() throws MarsRoverException {
		// testing User Story 1

		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");

		assertTrue(rover.planetContainsObstacleAt(4, 7));
		assertTrue(rover.planetContainsObstacleAt(2, 3));
		// assertFalse(rover.planetContainsObstacleAt(3, 3));
	}

	@Test
	public void testExecuteEmptyCommand() throws MarsRoverException {
		// testing User Story 2

		roverStatus = "(0,0,N)";
		command = "";

		assertEquals(roverStatus, rover.executeCommand(command));
	}

	@Test
	public void testExecuteTurningCommand() throws MarsRoverException {
		// testing User Story 3

		//Initial landing position

		// testing command "r"
		roverStatus = "(0,0,E)";
		command = "r";
		assertEquals("testExecuteTurningCommand(): Failure of the command \"r\" testing on N direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		roverStatus = "(0,0,S)";
		assertEquals("testExecuteTurningCommand(): Failure of the command \"r\" testing on E direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		roverStatus = "(0,0,W)";
		assertEquals("testExecuteTurningCommand(): Failure of the command \"l\" testing on S direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		roverStatus = "(0,0,N)";
		assertEquals("testExecuteTurningCommand(): Failure of the command \"l\" testing on W direction.", roverStatus,
				rover.executeCommand(command));

		// testing command "l"

		rover.setStatus("(0,0,N)"); // set the position to (0,0,N), as required bu the User Story example

		roverStatus = "(0,0,W)";
		command = "l";
		assertEquals("testExecuteTurningCommand(): Failure of the command \"l\" testing on N direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		roverStatus = "(0,0,S)";
		assertEquals("testExecuteTurningCommand(): Failure of the command \"l\" testing on W direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		roverStatus = "(0,0,E)";
		assertEquals("testExecuteTurningCommand(): Failure of the command \"l\" testing on S direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		roverStatus = "(0,0,N)";
		assertEquals("testExecuteTurningCommand(): Failure of the command \"l\" testing on E direction.", roverStatus,
				rover.executeCommand(command));
	}

	@Test
	public void testExecuteMovingForwardCommand() throws MarsRoverException {
		// testing User Story 4

		rover.setStatus("(7,6,N)");

		roverStatus = "(7,7,N)";
		command = "f";

		assertEquals(roverStatus, rover.executeCommand(command));
		// triangulation
		roverStatus = "(7,8,N)";
		assertEquals("testExecuteMovingForwardCommand(): Failure of the N direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		rover.setStatus("(7,6,E)");
		roverStatus = "(8,6,E)";
		assertEquals("testExecuteMovingBackwardCommand(): Failure of the N direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		rover.setStatus("(7,6,S)");
		roverStatus = "(7,5,S)";
		assertEquals("testExecuteMovingBackwardCommand(): Failure of the W direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		rover.setStatus("(7,6,W)");
		roverStatus = "(6,6,W)";
		assertEquals("testExecuteMovingBackwardCommand(): Failure of the S direction.", roverStatus,
				rover.executeCommand(command));
	}

	@Test
	public void testExecuteMovingBackwardCommand() throws MarsRoverException {
		// testing User Story 5

		rover.setStatus("(5,8,E)");

		roverStatus = "(4,8,E)";
		command = "b";

		assertEquals(roverStatus, rover.executeCommand(command));
		// triangulation
		roverStatus = "(3,8,E)";
		assertEquals("testExecuteMovingBackwardCommand(): Failure of the E direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		rover.setStatus("(5,8,N)");
		roverStatus = "(5,7,N)";
		assertEquals("testExecuteMovingBackwardCommand(): Failure of the N direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		rover.setStatus("(5,8,W)");
		roverStatus = "(6,8,W)";
		assertEquals("testExecuteMovingBackwardCommand(): Failure of the W direction.", roverStatus,
				rover.executeCommand(command));
		// triangulation
		rover.setStatus("(5,8,S)");
		roverStatus = "(5,9,S)";
		assertEquals("testExecuteMovingBackwardCommand(): Failure of the S direction.", roverStatus,
				rover.executeCommand(command));
	}

	@Test
	public void testExecuteMovingCombinedCommand() throws MarsRoverException {
		// testing User Story 6

		//Initial landing position

		roverStatus = "(2,2,E)";
		command = "ffrff";

		assertEquals(roverStatus, rover.executeCommand(command));

		// triangulation
		rover.setStatus("(0,0,N)");

		roverStatus = "(1,2,N)";
		command = "ffrffbl";

		assertEquals(roverStatus, rover.executeCommand(command));
	}

	@Test
	public void testWrapping() throws MarsRoverException {
		// testing User Story 7

		//Initial landing position

		roverStatus = "(0,9,N)";
		command = "b";

		assertEquals(roverStatus, rover.executeCommand(command));
		
		// triangulation
		roverStatus = "(0,0,N)";
		command = "f";
		assertEquals(roverStatus, rover.executeCommand(command));

	}
	
	@Test
	public void testSingleObstacle() throws MarsRoverException {
		// testing User Story 8

		//Initial landing position

		roverStatus = "(1,2,E)(2,2)";
		command = "ffrfff";
		this.planetObstacles.add("(2,2)");

		assertEquals(roverStatus, rover.executeCommand(command));
		
		//triangulation
		rover.setStatus("(0,0,N)");
		
		roverStatus = "(0,2,E)(2,2)";
		command = "ffrffffb";

		assertEquals(roverStatus, rover.executeCommand(command));
	}
	
	@Test
	public void testMultipleObstacle() throws MarsRoverException {
		// testing User Story 9

		//Initial landing position

		this.planetObstacles.add("(2,2)");
		this.planetObstacles.add("(2,1)");
		
		roverStatus = "(1,1,E)(2,2)(2,1)";
		command = "ffrfffrflf";		

		assertEquals(roverStatus, rover.executeCommand(command));
	}
	
	@Test
	public void testWrappingAndObstacle() throws MarsRoverException {
		// testing User Story 10

		//Initial landing position

		this.planetObstacles.add("(0,9)");
		
		roverStatus = "(0,0,N)(0,9)";
		command = "b";		

		assertEquals(roverStatus, rover.executeCommand(command));
	}
	
	@Test
	public void testTourAroundThePlanet() throws MarsRoverException {
		// testing User Story 11

		//Initial landing position

		this.planetObstacles.add("(2,2)");
		this.planetObstacles.add("(0,5)");
		this.planetObstacles.add("(5,0)");
		rover = new MarsRover(6, 6, this.planetObstacles);		
		
		roverStatus = "(0,0,N)(2,2)(0,5)(5,0)";
		command = "ffrfffrbbblllfrfrbbl";		

		assertEquals(roverStatus, rover.executeCommand(command));
	}
	
	


}
