package tdd.training.mra;

public class MarsRoverStatus {

	public static enum Directions {N, E, S, W};
	//unsigned
	private int xPos;
	private int yPos;
	private Directions dir;
	
	
	public MarsRoverStatus(int xPos, int yPos, Directions dir) {
		super();
		this.xPos = xPos;
		this.yPos = yPos;
		this.dir = dir;
	}

	public int getXPos() {
		return this.xPos;
	}
	
	public void setXPos(int xPos) {
		this.xPos = xPos;
	}

	public int getYPos() {
		return this.yPos;
	}

	public void setYPos(int yPos) {
		this.yPos = yPos;
	}

	public Directions getDirection() {
		return dir;
	}

	public void setDirection(Directions dir) {
		this.dir = dir;
	}

	/**
	 * 
	 * @return x and y rover coordinates in "(x,y)" String format
	 */
	public String getCoordinates() {
		return "("+this.getXPos()+","+this.getYPos()+")";
	}
	
	public void setStatus(int xPos, int yPos, Directions dir) {
		this.xPos = xPos;
		this.yPos = yPos;
		this.dir = dir;
	}

	
	public String toString() {
		return "("+this.getXPos()+","+this.getYPos()+","+this.getDirection()+")";
	}
}
